<?php

namespace Model;

use Model\Base\BaseModel;

class OrderModel extends BaseModel
{
    public $id;
    public $customer_id;
    public $total_price;
    public $created;
    public $modified;
    public $status;

    public function getNombreTabla(): string
    {
        return 'orders';
    }

    public function getNombreCampoPrimario(): string
    {
        return 'id';
    }

    /**
     * Son los campos que se van a establecer dentro
     * de las consultas de insert y update
     * @return array
     */
    public function getCamposAGuardar(): array
    {
        return [
            'id',
            'customer_id',
            'total_price',
            'created',
            'modified',
            'status'
        ];
    }

    /**
     * Son los campos que se van a establecer dentro
     * de las consultas de select por id (para autocargado)
     * @return array
     */
    public function getCamposACargar(): array
    {
        return [
            'id',
            'customer_id',
            'total_price',
            'created',
            'modified',
            'status'
        ];
    }
}
