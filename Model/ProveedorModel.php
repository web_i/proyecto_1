<?php

namespace Model;

use Model\Base\BaseModel;

class ProveedorModel extends BaseModel
{
    public $idproveedor; // pk

    public $email;
    public $nombre;
    public $direccion;
    public $codproveedor;
    public $limite_credito;

    public function getNombreTabla(): string
    {
        return 'proveedores';
    }

    public function getNombreCampoPrimario(): string
    {
        return 'idproveedor';
    }

    /**
     * Son los campos que se van a establecer dentro
     * de las consultas de insert y update
     * @return array
     */
    public function getCamposAGuardar(): array
    {
        return [
            'email',
            'nombre',
            'direccion',
            'codproveedor'
        ];
    }

    /**
     * Son los campos que se van a establecer dentro
     * de las consultas de select por id (para autocargado)
     * @return array
     */
    public function getCamposACargar(): array
    {
        return [
            'email',
            'nombre',
            'direccion',
            'idproveedor',
            'codproveedor'
        ];
    }
}
