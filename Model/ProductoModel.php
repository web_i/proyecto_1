<?php

namespace Model;

use Model\Base\BaseModel;

class ProductoModel extends BaseModel
{
    public $idproducto;
    public $codproducto;
    public $nombre;
    public $descripcion;
    public $costo;
    public $precio;
    public $idcategoria;

    public function getNombreTabla(): string
    {
        return 'productos';
    }

    public function getNombreCampoPrimario(): string
    {
        return 'idproducto';
    }

    /**
     * Son los campos que se van a establecer dentro
     * de las consultas de insert y update
     * @return array
     */
    public function getCamposAGuardar(): array
    {
        return [
            'costo',
            'precio',
            'nombre',
            'codproducto',
            'descripcion',
            'idcategoria'
        ];
    }

    /**
     * Son los campos que se van a establecer dentro
     * de las consultas de select por id (para autocargado)
     * @return array
     */
    public function getCamposACargar(): array
    {
        return [
            'idproducto',
            'codproducto',
            'nombre',
            'descripcion',
            'costo',
            'precio',
            'idcategoria'
        ];
    }
}
