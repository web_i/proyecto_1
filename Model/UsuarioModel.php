<?php

namespace Model;

use Core\DataBaseWhere;
use Model\Base\BaseModel;
use Utils\Tools;

class UsuarioModel extends BaseModel
{
    /** @var int */
    public $idusuario;

    /** @var string */
    public $nombre;

    /** @var string */
    public $username;

    /** @var string */
    public $email;

    /** @var string */
    public $pass;

    /** @var string */
    public $rol;

    /** @var string date */
    public $fecha_creacion;

    /** @var string datetime */
    public $fecha_acceso;

    /** @var string datetime */
    public $idvehiculo;

    public function getNombreTabla(): string
    {
        return 'usuarios';
    }

    public function getNombreCampoPrimario(): string
    {
        return 'idusuario';
    }

    /**
     * Son los campos que se van a establecer dentro
     * de las consultas de insert y update
     * @return array
     */
    public function getCamposAGuardar(): array
    {
        return [
            'rol',
            'pass',
            'email',
            'nombre',
            'username',
            'fecha_acceso',
            'fecha_creacion'
        ];
    }

    /**
     * Son los campos que se van a establecer dentro
     * de las consultas de select por id (para autocargado)
     * @return array
     */
    public function getCamposACargar(): array
    {
        return [
            'idusuario',
            'nombre',
            'username',
            'rol',
            'email',
            'fecha_acceso',
            'fecha_creacion',
            'idvehiculo'
        ];
    }

    /** Encriptar contraseña */
    protected function getHashPass(string $pass): string
    {
        return password_hash($pass, PASSWORD_BCRYPT, ['cost' => 10]);
    }

    /** 
     * En este caso sobre escribimos el método guardar porque
     * deseamos encriptar la contraseña
     */
    public function guardar(): bool
    {
        if (!empty($this->pass))
            $this->pass = $this->getHashPass($this->pass);

        return parent::guardar();
    }

    /**
     * Verifica las credenciales del usuario
     * 
     * De ser valido carga los campos al modelo
     */
    public function autenticar(string $username, string $pass): bool
    {
        if (empty($username) || empty($pass))
            return false;

        $where = [
            new DataBaseWhere('username', $username)
        ];
        $respuesta = $this->obtenerTodos($where);

        if (empty($respuesta)) {
            Tools::ImprimirEnLog('UsuarioModel', 'autenticar', 'error 1', "Credenciales no concuerdan");
            return false;
        }

        // Cargamos los tados del usuario
        $usuario = $respuesta[0];
        if (!password_verify($pass, $usuario['pass'])) {
            Tools::ImprimirEnLog('UsuarioModel', 'autenticar', 'error 2', "Credenciales no concuerdan");
            return false;
        }

        $this->rellenarDatos($usuario);

        return true;
    }
}
