<?php

namespace Model;

use Model\Base\BaseModel;

class GrupoClienteModel extends BaseModel
{
    public $idgrupo;
    public $codgrupo;
    public $nombre;
    public $descripcion;

    public function getNombreTabla(): string
    {
        return 'grupos_clientes';
    }

    public function getNombreCampoPrimario(): string
    {
        return 'idgrupo';
    }

    /**
     * Son los campos que se van a establecer dentro
     * de las consultas de insert y update
     * @return array
     */
    public function getCamposAGuardar(): array
    {
        return [
            'codgrupo',
            'nombre',
            'descripcion'
        ];
    }

    /**
     * Son los campos que se van a establecer dentro
     * de las consultas de select por id (para autocargado)
     * @return array
     */
    public function getCamposACargar(): array
    {
        return [
            'idgrupo',
            'codgrupo',
            'nombre',
            'descripcion'
        ];
    }
}
