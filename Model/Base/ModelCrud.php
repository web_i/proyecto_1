<?php

namespace Model\Base;

use Core\DataBase;
use Core\DataBaseWhere;
use Model\Base\ModelRequire;

/**
 * Encargado de realizar la conexión con las bases de datos
 */
abstract class ModelCrud extends ModelRequire
{
    /**
     * @param DataBaseWhere[]
     */
    public function siExiste(array $initial_wheres = []): bool
    {
        $query = "SELECT 1 FROM {$this->getNombreTabla()} WHERE ";

        // Si no se obtienen wheres, se usara el campo primario como where
        if (empty($initial_wheres)) {
            $id = $this->getValorCampoPrimario();

            // Si se va a usar la llave primaria, se valida que no sea nula
            if (empty($id))
                return false;

            $query .= "{$this->getNombreCampoPrimario()} = '{$id}'";
        }

        // Si se obtienen wheres, esos se usaran para el query en la sección del where
        if (!empty($initial_wheres)) {
            /** @var DataBaseWhere */
            foreach ($initial_wheres as $key => $where) {
                if ($key !== array_key_last($initial_wheres))
                    $query .= $where->getExtension();

                $query .= (string) $where;
            }
        }

        $database = new DataBase();
        $resultado = $database->query($query);

        if (false === $resultado)
            return false;

        $cantidadRegistros = mysqli_num_rows($resultado);

        return ($cantidadRegistros > 0);
    }

    protected function actualizar(): bool
    {
        $database = new DataBase();
        return $database->actualizar($this->getNombreTabla(), $this->getUpdateArray());
    }

    protected function crear(): bool
    {
        $database = new DataBase();
        return $database->insertar($this->getNombreTabla(), $this->getInsertArray());
    }

    public function eliminar(): bool
    {
        $id = $this->getValorCampoPrimario();

        if (empty($id))
            return false;

        $database = new DataBase();
        return $database->eliminar($this->getNombreTabla(), $this->getNombreCampoPrimario(), $id);
    }

    public function obtenerTodos(array $initial_wheres = [])
    {
        $wheres = [];

        foreach ($initial_wheres as $where)
            $wheres[] = $where;

        $database = new DataBase();
        return $database->seleccionar($this->getNombreTabla(), $wheres);
    }

    public function cargarPorId(string $id = '', array $initial_wheres = []): bool
    {
        $wheres = [];

        if (!empty($id))
            $wheres[] = new DataBaseWhere($this->getNombreCampoPrimario(), $id);

        foreach ($initial_wheres as $where)
            $wheres[] = $where;

        $database = new DataBase();
        $data = $database->seleccionar($this->getNombreTabla(), $wheres);

        if (empty($data))
            return false;

        $this->rellenarDatos($data[0]);
        return true;
    }
}
