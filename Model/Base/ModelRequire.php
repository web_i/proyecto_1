<?php

namespace Model\Base;

/**
 * Clase encargada de solicitar a los modelos el crear ciertas funciones
 */
abstract class ModelRequire
{
    /** Debe retornar el nombre la tabla del modelo */
    abstract public function getNombreTabla(): string;

    /** Debe retornar el nombre del campo "llave primaria" */
    abstract public function getNombreCampoPrimario(): string;

    /** Debe retornar un string con el nombre de los campos que conecten a la bd */
    abstract protected function getCamposAGuardar(): array;

    /** Debe retornar un string con el nombre de los campos que se desean cargar desde la bd */
    abstract protected function getCamposACargar(): array;

    /** 
     * Retorna el valor del campo primario
     * en caso no encontrar el campo retornará un null
     */
    public function getValorCampoPrimario()
    {
        $nombreCampo = $this->getNombreCampoPrimario();
        return $this->$nombreCampo ?? null;
    }

    /** Genera el array a utilizar en el insert */
    protected function getInsertArray(): array
    {
        $modeloArray = $this->getCamposAGuardar();

        // Generamos el query de los campos y valores
        $columnas = '(';
        $valores = '(';
        foreach ($modeloArray as $campo) {
            if (empty($this->$campo))
                continue;

            $columnas .= $campo;
            $valores .= "'" . $this->$campo . "'";

            // Si hay más campos, se agregará la "," para la consulta
            if ($campo !== array_key_last($modeloArray)) {
                $columnas .= ',';
                $valores .= ',';
            }
        }
        $valores .= ')';
        $columnas .= ')';

        $columnas = str_replace(",''", '', $columnas);
        $columnas = str_replace(",)", ')', $columnas);
        $valores = str_replace(",''", '', $valores);
        $valores = str_replace(",)", ')', $valores);

        return [
            'columnas' => $columnas,
            'valores' => $valores
        ];
    }

    /** Genera el array a utilizar en el insert */
    protected function getUpdateArray(): array
    {
        $modeloArray = $this->getCamposAGuardar();

        // Generamos el query de los campos y valores
        $valores = '';
        foreach ($modeloArray as $campo) {
            if (empty($this->$campo))
                continue;

            $valores .= $campo . '=' . "'" . $this->$campo . "'";

            // Si hay más campos, se agregará la "," para la consulta
            if ($campo !== array_key_last($modeloArray))
                $valores .= ',';
        }
        $valores .= '';

        return [
            'valores' => $valores,
            'id' => $this->getValorCampoPrimario(),
            'nombrecampo' => $this->getNombreCampoPrimario()
        ];
    }

    /**
     * Se encarga de cargas los datos que se reciben desde un array
     * a los atributos del modelo
     */
    protected function rellenarDatos(array &$data)
    {
        foreach ($this->getCamposACargar() as $campo)
            $this->$campo = $data[$campo];
    }
}
