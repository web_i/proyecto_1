<?php

namespace Model\Base;

use Utils\Tools;
use Model\Base\ModelCrud;

abstract class BaseModel extends ModelCrud
{
    /** Se encarga de realizar las validaciones para le modelo */
    protected function test(): bool
    {
        return true;
    }

    /** Se encarga de guardar/actualizar un objeto */
    public function guardar(): bool
    {
        if (false === $this->test())
            return false;

        if ($this->siExiste())
            return $this->actualizar();

        return $this->crear();
    }
}
