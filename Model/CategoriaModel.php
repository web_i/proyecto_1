<?php

namespace Model;

use Model\Base\BaseModel;

class CategoriaModel extends BaseModel
{
    public $idcategoria;
    public $codcategoria;
    public $nombre;
    public $descripcion;

    public function getNombreTabla(): string
    {
        return 'categorias_productos';
    }

    public function getNombreCampoPrimario(): string
    {
        return 'idcategoria';
    }

    /**
     * Son los campos que se van a establecer dentro
     * de las consultas de insert y update
     * @return array
     */
    public function getCamposAGuardar(): array
    {
        return [
            'codcategoria',
            'nombre',
            'descripcion'
        ];
    }

    /**
     * Son los campos que se van a establecer dentro
     * de las consultas de select por id (para autocargado)
     * @return array
     */
    public function getCamposACargar(): array
    {
        return [
            'idcategoria',
            'codcategoria',
            'nombre',
            'descripcion'
        ];
    }
}
