<?php

namespace Model;

use Model\Base\BaseModel;

class ClienteModel extends BaseModel
{
    public $id;
    public $name;
    public $email;
    public $phone;
    public $address;
    public $created;
    public $modified;
    public $status;

    public function getNombreTabla(): string
    {
        return 'customers';
    }

    public function getNombreCampoPrimario(): string
    {
        return 'id';
    }

    /**
     * Son los campos que se van a establecer dentro
     * de las consultas de insert y update
     * @return array
     */
    public function getCamposAGuardar(): array
    {
        return [
            'id',
            'name',
            'email',
            'phone',
            'address',
            'created',
            'modified',
            'status'
        ];
    }

    /**
     * Son los campos que se van a establecer dentro
     * de las consultas de select por id (para autocargado)
     * @return array
     */
    public function getCamposACargar(): array
    {
        return [
            'id',
            'name',
            'email',
            'phone',
            'address',
            'status'
        ];
    }
}
