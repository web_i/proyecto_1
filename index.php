<?php

use Core\App;

require_once './Config/config.php';

#region Manejo de rutas
/**
 * Cada vez que se instancie un archivo se va a ejecutar esta sección
 * 
 * Las rutas se van a manejar con:
 * - namespace : para establecer la ruta en la que se encuentra un archivo
 * - use : para cargar un archivo
 */
spl_autoload_register(function ($clase) {
    require_once str_replace('\\', '/', $clase) . '.php';
});
#endregion

#region Carga de aplicación web
$app = new App();
#endregion
