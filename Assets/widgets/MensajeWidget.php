<?php

namespace Assets\widgets;

use Utils\Traducciones;

class MensajeWidget
{
    public static function render(string $tipo_mensaje, string $contenido, bool $se_traduce = false)
    {
        if ($se_traduce) {
            $tools = new Traducciones();
            $contenido = $tools->trans($contenido);
        }

        echo '<div class="mensaje_div-' . $tipo_mensaje . '">'
            . '<p class="mensaje-' . $tipo_mensaje . '">' . $contenido . '</p>'
            . '</div>';
    }
}
