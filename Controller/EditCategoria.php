<?php

namespace Controller;

use Assets\widgets\MensajeWidget;
use Controller\Base\SessionController;
use Core\DataBaseWhere;
use Model\CategoriaModel;
use Utils\TipoMensaje;
use Utils\Tools;

class EditCategoria extends SessionController
{
    /**
     * It returns the name of the model that will be used to store the data
     * 
     * @return string The name of the model.
     */
    public function getModeloPrincipal(): string
    {
        return '';
    }

    /**
     * Como se va a utilizar una vista distinta al index, cargo el render
     */
    public function render()
    {
        $this->view->render($this->getVistaPrincipal(), [], false);
        Tools::ImprimirEnLog($this->getClassDireccion(), 'render', TipoMensaje::Seguimiento->value, 'Carga de la vista (' . $this->getVistaPrincipal() . ')');
    }

    public function getVistaPrincipal(): string
    {
        return 'categorias/edit_categoria';
    }

    protected function getClassDireccion(): string
    {
        return (string) static::class;
    }

    protected function aplicarAccion(string $accion, array $data = [], string $vista = '')
    {
        switch ($accion) {
            case 'crear_categoria':
                return $this->crearCategoria($data);
        }
    }

    private function crearCategoria(array &$data)
    {
        $categoria = new CategoriaModel();
        $mensaje = new MensajeWidget();

        $categoria->nombre = $data['nombre'] ?? null;
        $categoria->codcategoria = $data['codcategoria'] ?? null;
        $categoria->descripcion = $data['descripcion'] ?? null;

        // Verificamos si el usuario ya fue creado
        $where = [new DataBaseWhere('codcategoria', $categoria->codcategoria)];
        if ($categoria->siExiste($where)) {
            Tools::ImprimirEnLog($this->getClassDireccion(), 'crearCategoria', TipoMensaje::Advertencia->value, "La categoria ({$categoria->codcategoria}) fue creada anteriormente");
            $mensaje::render(TipoMensaje::Advertencia->value, "La categoria ({$categoria->codcategoria}) fue creado anteriormente");
            return;
        }

        if ($categoria->guardar()) {
            Tools::ImprimirEnLog($this->getClassDireccion(), 'crearCategoria', TipoMensaje::Confirmacion->value, 'Se creo la categoria');
            $mensaje::render(TipoMensaje::Confirmacion->value, "Se ha completado el registro de la categoria ({$categoria->codcategoria})");
            return;
        }

        Tools::ImprimirEnLog($this->getClassDireccion(), 'crearCategoria', TipoMensaje::Error->value, "No se pudó crear la categoria ({$categoria->codcategoria})");
        $mensaje::render(TipoMensaje::Error->value, "No se pudó crear la categoria ({$categoria->codcategoria})");
        return;
    }
}
