<?php

namespace Controller;

use Assets\widgets\MensajeWidget;
use Controller\Base\SessionController;
use Core\DataBaseWhere;
use Model\UsuarioModel;
use Utils\TipoMensaje;
use Utils\Tools;

class Login extends SessionController
{
    /**
     * It returns the name of the model that will be used to store the data
     * 
     * @return string The name of the model.
     */
    public function getModeloPrincipal(): string
    {
        return '';
    }

    public function getVistaPrincipal(): string
    {
        return 'login';
    }

    protected function getClassDireccion(): string
    {
        return (string) static::class;
    }

    protected function aplicarAccion(string $accion, array $data = [], string $vista = '')
    {
        switch ($accion) {
            case 'iniciar_sesion':
                return $this->validarCredenciales($data);
        }
    }

    private function validarCredenciales(array &$data)
    {
        $usuario = new UsuarioModel();

        $username = $data['username'] ?? null;
        $pass = $data['pass'] ?? null;

        if (empty($username) || empty($pass)) {
            MensajeWidget::render(TipoMensaje::Advertencia->value, 'Debe ingresar ambas credenciales');
            return;
        }

        if (!$usuario->autenticar($username, $pass)) {
            MensajeWidget::render(TipoMensaje::Advertencia->value, 'Credenciales no coinciden');
            return;
        }

        // Cargamos los datos del usuario
        $where = [new DataBaseWhere('username', $username)];
        $usuario->cargarPorId('', $where);

        $this->iniciarSesion($usuario);
    }
}
