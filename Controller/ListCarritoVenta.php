<?php

namespace Controller;

use Controller\Base\SessionController;
use Utils\Carrito;
use Utils\Database_2;
use Utils\TipoMensaje;
use Utils\Tools;

class CarritoVenta extends SessionController
{
    /**
     * It returns the name of the model that will be used to store the data
     * 
     * @return string The name of the model.
     */
    public function getModeloPrincipal(): string
    {
        return '';
    }

    public function getVistaPrincipal(): string
    {
        return 'ventas/viewCart';
    }

    /**
     * Como se va a utilizar una vista distinta al index, cargo el render
     */
    public function render()
    {
        $accion = $_REQUEST['action'];
        if (!empty($accion)) {
            $this->aplicarAccion($accion, $_POST);
            unset($_REQUEST['action']);
        }
        $this->view->render($this->getVistaPrincipal(), [], false);
        Tools::ImprimirEnLog($this->getClassDireccion(), 'render', TipoMensaje::Seguimiento->value, 'Carga de la vista (' . $this->getVistaPrincipal() . ')');
    }

    protected function getClassDireccion(): string
    {
        return (string) static::class;
    }

    protected function aplicarAccion(string $accion, array $data = [], string $vista = '')
    {
    }
}
