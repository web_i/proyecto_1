<?php

namespace Controller;

use Controller\Base\SessionController;

use Utils\TipoMensaje;
use Utils\Tools;

class Checkout extends SessionController
{
    public function __construct()
    {
        parent::__construct();

        $accion = $_REQUEST['action'] ?? null;
        if (!empty($accion)) {
            $this->aplicarAccion($accion, $_POST);
            unset($_REQUEST['action']);
        }
    }

    /**
     * It returns the name of the model that will be used to store the data
     * 
     * @return string The name of the model.
     */
    public function getModeloPrincipal(): string
    {
        return '';
    }

    public function getVistaPrincipal(): string
    {
        return 'ventas/checkout';
    }

    /**
     * Como se va a utilizar una vista distinta al index, cargo el render
     */
    public function render()
    {
        $this->view->render($this->getVistaPrincipal(), [], false);
        Tools::ImprimirEnLog($this->getClassDireccion(), 'render', TipoMensaje::Seguimiento->value, 'Carga de la vista (' . $this->getVistaPrincipal() . ')');
    }

    protected function getClassDireccion(): string
    {
        return (string) static::class;
    }

    protected function aplicarAccion(string $accion, array $data = [], string $vista = '')
    {
    }
}
