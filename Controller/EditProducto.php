<?php

namespace Controller;

use Assets\widgets\MensajeWidget;
use Controller\Base\SessionController;
use Core\DataBaseWhere;
use Model\ProductoModel;
use Utils\TipoMensaje;
use Utils\Tools;

class EditProducto extends SessionController
{
    /**
     * It returns the name of the model that will be used to store the data
     * 
     * @return string The name of the model.
     */
    public function getModeloPrincipal(): string
    {
        return '';
    }

    /**
     * Como se va a utilizar una vista distinta al index, cargo el render
     */
    public function render()
    {
        $this->view->render($this->getVistaPrincipal(), [], false);
        Tools::ImprimirEnLog($this->getClassDireccion(), 'render', TipoMensaje::Seguimiento->value, 'Carga de la vista (' . $this->getVistaPrincipal() . ')');
    }

    public function getVistaPrincipal(): string
    {
        return 'productos/edit_producto';
    }

    protected function getClassDireccion(): string
    {
        return (string) static::class;
    }

    protected function aplicarAccion(string $accion, array $data = [], string $vista = '')
    {
        switch ($accion) {
            case 'crear_producto':
                return $this->crearProducto($data);
        }
    }

    private function crearProducto(array &$data)
    {
        $producto = new ProductoModel();
        $mensaje = new MensajeWidget();

        $producto->costo = $data['costo'] ?? null;
        $producto->nombre = $data['nombre'] ?? null;
        $producto->precio = $data['precio'] ?? null;
        $producto->codproducto = $data['codproducto'] ?? null;
        $producto->descripcion = $data['descripcion'] ?? null;
        $producto->idcategoria = $data['idcategoria'] ?? null;

        // Verificamos si el usuario ya fue creado
        $where = [new DataBaseWhere('codproducto', $producto->codproducto)];
        if ($producto->siExiste($where)) {
            Tools::ImprimirEnLog($this->getClassDireccion(), 'crearProducto', TipoMensaje::Advertencia->value, "El producto ({$producto->codproducto}) fue creado anteriormente");
            $mensaje::render(TipoMensaje::Advertencia->value, "El producto ({$producto->codproducto}) fue creado anteriormente");
            return;
        }

        if ($producto->guardar()) {
            Tools::ImprimirEnLog($this->getClassDireccion(), 'crearProducto', TipoMensaje::Confirmacion->value, 'Se creo el producto');
            $mensaje::render(TipoMensaje::Confirmacion->value, "Se ha completado el registro del producto ({$producto->codproducto})");
            return;
        }

        Tools::ImprimirEnLog($this->getClassDireccion(), 'crearProducto', TipoMensaje::Error->value, "No se pudó crear al producto ({$producto->codproducto})");
        $mensaje::render(TipoMensaje::Error->value, "No se pudó crear el producto ({$producto->codproducto})");
        return;
    }
}
