<?php

namespace Controller;

use Assets\widgets\MensajeWidget;
use Controller\Base\SessionController;
use Core\DataBaseWhere;
use Model\ProveedorModel;
use Utils\TipoMensaje;
use Utils\Tools;

class EditProveedor extends SessionController
{
    /**
     * It returns the name of the model that will be used to store the data
     * 
     * @return string The name of the model.
     */
    public function getModeloPrincipal(): string
    {
        return '';
    }

    /**
     * Como se va a utilizar una vista distinta al index, cargo el render
     */
    public function render()
    {
        $this->view->render($this->getVistaPrincipal(), [], false);
        Tools::ImprimirEnLog($this->getClassDireccion(), 'render', TipoMensaje::Seguimiento->value, 'Carga de la vista (' . $this->getVistaPrincipal() . ')');
    }

    public function getVistaPrincipal(): string
    {
        return 'proveedores/edit_proveedor';
    }

    protected function getClassDireccion(): string
    {
        return (string) static::class;
    }

    protected function aplicarAccion(string $accion, array $data = [], string $vista = '')
    {
        switch ($accion) {
            case 'crear_proveedor':
                return $this->crearProveedor($data);
        }
    }

    private function crearProveedor(array &$data)
    {
        $proveedor = new ProveedorModel();
        $mensaje = new MensajeWidget();

        $proveedor->email = $data['email'] ?? null;
        $proveedor->nombre = $data['nombre'] ?? null;
        $proveedor->direccion = $data['direccion'] ?? null;
        $proveedor->codproveedor = $data['codproveedor'] ?? null;

        // Verificamos si el usuario ya fue creado
        $where = [new DataBaseWhere('codproveedor', $proveedor->codproveedor)];
        if ($proveedor->siExiste($where)) {
            Tools::ImprimirEnLog($this->getClassDireccion(), 'crearProducto', TipoMensaje::Advertencia->value, "El proveedor ({$proveedor->codproveedor}) fue creado anteriormente");
            $mensaje::render(TipoMensaje::Advertencia->value, "El proveedor ({$proveedor->codproveedor}) fue creado anteriormente");
            return;
        }

        if ($proveedor->guardar()) {
            Tools::ImprimirEnLog($this->getClassDireccion(), 'crearProveedor', TipoMensaje::Confirmacion->value, 'Se creo el proveedor');
            $mensaje::render(TipoMensaje::Confirmacion->value, "Se ha completado el registro del proveedor ({$proveedor->codproveedor})");
            return;
        }

        Tools::ImprimirEnLog($this->getClassDireccion(), 'crearProveedor', TipoMensaje::Error->value, "No se pudó crear al proveedor ({$proveedor->codproveedor})");
        $mensaje::render(TipoMensaje::Error->value, "No se pudó crear el proveedor ({$proveedor->codproveedor})");
        return;
    }
}
