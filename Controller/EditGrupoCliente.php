<?php

namespace Controller;

use Assets\Widgets\MensajeWidget;
use Controller\Base\SessionController;
use Core\DataBaseWhere;
use Model\GrupoClienteModel;
use Model\UsuarioModel;
use Utils\TipoMensaje;
use Utils\Tools;

class EditGrupoCliente extends SessionController
{
    /**
     * It returns the name of the model that will be used to store the data
     * 
     * @return string The name of the model.
     */
    public function getModeloPrincipal(): string
    {
        return '';
    }

    public function getVistaPrincipal(): string
    {
        return 'clientes/edit_grupo_cliente';
    }

    /**
     * Como se va a utilizar una vista distinta al index, cargo el render
     */
    public function render()
    {
        $this->view->render($this->getVistaPrincipal(), [], false);
        Tools::ImprimirEnLog($this->getClassDireccion(), 'render', TipoMensaje::Seguimiento->value, 'Carga de la vista (' . $this->getVistaPrincipal() . ')');
    }

    protected function getClassDireccion(): string
    {
        return (string) static::class;
    }

    protected function aplicarAccion(string $accion, array $data = [], string $vista = '')
    {
        switch ($accion) {
            case 'crear_grupo_cliente':
                return $this->crearGrupo($data);
        }
    }

    private function crearGrupo(array &$data)
    {
        $grupo = new GrupoClienteModel();
        $mensaje = new MensajeWidget();

        $grupo->nombre = $data['nombre'] ?? null;
        $grupo->codgrupo = $data['codgrupo'] ?? null;
        $grupo->descripcion = $data['descripcion'] ?? null;

        // Verificamos si el usuario ya fue creado
        $where = [new DataBaseWhere('codgrupo', $grupo->codgrupo)];
        if ($grupo->siExiste($where)) {
            Tools::ImprimirEnLog($this->getClassDireccion(), 'crearGrupo', TipoMensaje::Advertencia->value, " ({$grupo->codgrupo}) fue creado anteriormente");
            $mensaje::render(TipoMensaje::Advertencia->value, "({$grupo->codgrupo}) fue creado anteriormente");
            return;
        }

        if ($grupo->guardar()) {
            Tools::ImprimirEnLog($this->getClassDireccion(), 'crearGrupo', TipoMensaje::Confirmacion->value, 'Se creó');
            $mensaje::render(TipoMensaje::Confirmacion->value, "Se ha completado el registro de ({$grupo->codgrupo})");
            return;
        }

        Tools::ImprimirEnLog($this->getClassDireccion(), 'crearGrupo', TipoMensaje::Error->value, "No se pudó crear ({$grupo->codgrupo})");
        $mensaje::render(TipoMensaje::Error->value, "No se pudó crea ({$grupo->codgrupo})");
        return;
    }
}
