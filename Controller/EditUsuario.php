<?php

namespace Controller;

use Assets\Widgets\MensajeWidget;
use Controller\Base\SessionController;
use Core\DataBaseWhere;
use Model\UsuarioModel;
use Utils\TipoMensaje;
use Utils\Tools;

class EditUsuario extends SessionController
{
    /**
     * It returns the name of the model that will be used to store the data
     * 
     * @return string The name of the model.
     */
    public function getModeloPrincipal(): string
    {
        return '';
    }

    public function getVistaPrincipal(): string
    {
        return 'edit_usuario';
    }

    protected function getClassDireccion(): string
    {
        return (string) static::class;
    }

    protected function aplicarAccion(string $accion, array $data = [], string $vista = '')
    {
        switch ($accion) {
            case 'crear_usuario':
                return $this->crearUsuario($data);
        }
    }

    private function crearUsuario(array &$data)
    {
        $usuario = new UsuarioModel();
        $mensaje = new MensajeWidget();

        $usuario->nombre = $data['nombre'] ?? null;
        $usuario->username = $data['username'] ?? null;
        $usuario->email = $data['email'] ?? null;
        $usuario->pass = $data['pass'] ?? null;
        $usuario->rol = $data['rol'] ?? null;

        // Verificamos si el usuario ya fue creado
        $where = [new DataBaseWhere('username', $usuario->username)];
        if ($usuario->siExiste($where)) {
            Tools::ImprimirEnLog($this->getClassDireccion(), 'crearUsuario', TipoMensaje::Advertencia->value, "El usuario ({$usuario->username}) fue creado anteriormente");
            $mensaje::render(TipoMensaje::Advertencia->value, "El usuario ({$usuario->username}) fue creado anteriormente");
            return;
        }

        if ($usuario->guardar()) {
            Tools::ImprimirEnLog($this->getClassDireccion(), 'crearUsuario', TipoMensaje::Confirmacion->value, 'Se creo el usuario');
            $mensaje::render(TipoMensaje::Confirmacion->value, "Se ha completado el registro del usuario ({$usuario->username})");
            return;
        }

        Tools::ImprimirEnLog($this->getClassDireccion(), 'crearUsuario', TipoMensaje::Error->value, "No se pudó crear al usuario ({$usuario->username})");
        $mensaje::render(TipoMensaje::Error->value, "No se pudó crear el usuario ({$usuario->username})");
        return;
    }
}
