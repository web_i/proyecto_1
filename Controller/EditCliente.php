<?php

namespace Controller;

use Assets\widgets\MensajeWidget;
use Controller\Base\SessionController;
use Core\DataBaseWhere;
use Model\ClienteModel;
use Utils\TipoMensaje;
use Utils\Tools;

class EditCliente extends SessionController
{
    /**
     * It returns the name of the model that will be used to store the data
     * 
     * @return string The name of the model.
     */
    public function getModeloPrincipal(): string
    {
        return '';
    }

    /**
     * Como se va a utilizar una vista distinta al index, cargo el render
     */
    public function render()
    {
        $this->view->render($this->getVistaPrincipal(), [], false);
        Tools::ImprimirEnLog($this->getClassDireccion(), 'render', TipoMensaje::Seguimiento->value, 'Carga de la vista (' . $this->getVistaPrincipal() . ')');
    }

    public function getVistaPrincipal(): string
    {
        return 'clientes/edit_cliente';
    }

    protected function getClassDireccion(): string
    {
        return (string) static::class;
    }

    protected function aplicarAccion(string $accion, array $data = [], string $vista = '')
    {
        switch ($accion) {
            case 'crear_cliente':
                return $this->crearProducto($data);
        }
    }

    private function crearProducto(array &$data)
    {
        $cliente = new ClienteModel();
        $mensaje = new MensajeWidget();

        $cliente->name = $data['name'] ?? null;
        $cliente->email = $data['email'] ?? null;
        $cliente->phone = $data['phone'] ?? null;
        $cliente->address = $data['address'] ?? null;
        
        $cliente->status = 1;
        $cliente->created = date('d-m-Y H:i:s');
        $cliente->modified = date('d-m-Y H:i:s');

        if ($cliente->guardar()) {
            Tools::ImprimirEnLog($this->getClassDireccion(), 'crearCliente', TipoMensaje::Confirmacion->value, 'Se creo el cliente');
            $mensaje::render(TipoMensaje::Confirmacion->value, "Se ha completado el registro del cliente ({$cliente->name})");
            return;
        }

        Tools::ImprimirEnLog($this->getClassDireccion(), 'crearCliente', TipoMensaje::Error->value, "No se pudó crear al cliente ({$cliente->name})");
        $mensaje::render(TipoMensaje::Error->value, "No se pudó crear el cliente ({$cliente->name})");
        return;
    }
}
