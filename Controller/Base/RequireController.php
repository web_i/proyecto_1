<?php

namespace Controller\Base;

abstract class RequireController
{
    /**
     * Función encargada de retornar el nombre del modelo
     * que se debe cargar en el controlador
     * 
     * @return string
     */
    abstract public function getModeloPrincipal(): string;

    /**
     * Función encargada de retornar el nombre de la vista
     * que se debe cargar para este controlador
     * 
     * @return string
     */
    abstract public function getVistaPrincipal(): string;

    /** Debe retornar dirección de la clase 
     * @return string
     */
    abstract protected function getClassDireccion(): string;

    /** 
     * @return
     */
    abstract protected function aplicarAccion(string $accion, array $data = [], string $vista = '');
}
