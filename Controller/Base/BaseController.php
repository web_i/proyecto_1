<?php

namespace Controller\Base;

use Core\View;
use Utils\Tools;
use Utils\TipoMensaje;

abstract class BaseController extends RequireController
{
    public function __construct()
    {
        $this->view = new View();
        $this->tools = new Tools();
        Tools::ImprimirEnLog('BaseController', 'construct', TipoMensaje::Seguimiento->value, 'Instancia de controlador');

        $accion = $this->requestPOST('accion');
        if (!empty($accion)) {
            $this->aplicarAccion($accion, $_POST);
            unset($_POST['accion']);
        }
    }

    /** Carga el modelo principal */
    function loadModel($nombre_modelo = '')
    {
        if (empty($nombre_modelo)) {
            Tools::ImprimirEnLog($this->getClassDireccion(), 'loadModel', TipoMensaje::Seguimiento->value, "El controlador no posee ningún modelo por defecto");
            return;
        }

        $url = 'Model/' . $nombre_modelo . 'Model.php';
        if (!file_exists($url)) {
            Tools::ImprimirEnLog($this->getClassDireccion(), 'loadModel', TipoMensaje::Seguimiento->value, "No existe el modelo ({$url})");
            return;
        }

        Tools::ImprimirEnLog($this->getClassDireccion(), 'loadModel', TipoMensaje::Seguimiento->value, "Se ha cargado el modelo ({$url})");
        $rutaModelo = 'Model\\' . $nombre_modelo . 'Model';
        $this->modelo = new $rutaModelo();
    }

    /** Carga la vista del controlador */
    public function render()
    {
        $this->view->render($this->getVistaPrincipal());
        Tools::ImprimirEnLog($this->getClassDireccion(), 'render', TipoMensaje::Seguimiento->value, 'Carga de la vista (' . $this->getVistaPrincipal() . ')');
    }

    /** Obtiene el valor mandado por método GET */
    protected function requestGet($nombre)
    {
        return $_GET[$nombre] ?? null;
    }

    /** Obtiene el valor mandado por método POST */
    protected function requestPOST($nombre)
    {
        return $_POST[$nombre] ?? null;
    }

    /**
     * Redirecciona la vista a otro controlador
     */
    public function redirect(string $ruta, $mensajes = [])
    {
        $data = [];
        $params = '';

        foreach ($mensajes as $key => $mensaje)
            $data[] = $key . '=' . $mensaje;

        $params = join('&', $data);
        if (!empty($params))
            $params = '?' . $params;

        header('Location: ' . constant('URL') . $ruta . $params);
    }
}
