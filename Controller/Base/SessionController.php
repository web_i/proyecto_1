<?php

namespace Controller\Base;

use Controller\Base\BaseController;
use Core\DataBaseWhere;
use Core\Sesion;
use Utils\Tools;
use Model\UsuarioModel;
use Utils\TipoMensaje;

abstract class SessionController extends BaseController
{
    /** @var UsuarioModel */
    protected $usuario;

    /** @var Sesion */
    protected $sesion;

    /** Listado de las rutas con base en permisos */
    private $sitios;

    /** Listado de las rutas predeterminadas por rol */
    private $sitios_predeterminados;

    function __construct()
    {
        parent::__construct();
        $this->init();
    }

    /**
     * Inicia la instancia de la sesión
     */
    protected function iniciarSesion(UsuarioModel $usuario)
    {
        /**
         * Si es en base a una acción, 
         * desde el Base controller se movera a esta fase
         * por lo que se inicia la sesión
         */
        $this->init();

        Tools::ImprimirEnLog('SessionController', 'iniciarSesion', TipoMensaje::Seguimiento->value, 'Se ha iniciado sesión correctamente');
        $this->sesion->setUsuarioActual($usuario->idusuario);

        $ruta = $this->sitios_predeterminados[$usuario->rol];
        $this->redirect($ruta);
    }

    /**
     * Cargamos todo lo relacionado con la sesión
     */
    protected function init()
    {
        Tools::ImprimirEnLog('SessionController', 'init', TipoMensaje::Seguimiento->value, 'Se cargan los datos de la sesión');

        $this->sesion = new Sesion();
        $this->usuario = new UsuarioModel();

        // Se cargan las rutas / permisos
        $rutas = $this->getJsonRutas();
        $this->sitios = $rutas['sites'];
        $this->sitios_predeterminados = $rutas['default-sites'];

        // Validamos rol y permisos
        $this->validarSesion();
    }

    /** Carga y formatea a un array asociativo el archivo de rutas posibles */
    private function getJsonRutas()
    {
        $string = file_get_contents("./Config/accesos.json");
        $json = json_decode($string, true);
        return $json;
    }

    /** 
     * Se encarga de validar sesión para 
     * determinar a que páginas se puede ingresar 
     */
    private function validarSesion()
    {
        // Si existe una sesión
        if ($this->existeSesion())
            return $this->manejarRutasConSesion();

        $this->manejarRutasSinSesion();
    }

    public function cerrarSesion()
    {
        $this->sesion->terminarSesion();
        $this->redirect('ZAPATO');
    }

    /**
     * Realiza la comprobación y redirección tomando en cuenta
     * la sesión que existe
     */
    private function manejarRutasConSesion()
    {
        Tools::ImprimirEnLog('SessionController', 'manejarRutasConSesion', TipoMensaje::Seguimiento->value, 'Existe sesión');

        $paginaActual = $this->getPaginaActual();
        $paginaActual = preg_replace("/\?.*/", "", $paginaActual); //omitir get info

        // Si no existe el usuario por alguna razón cerraremos la sesión
        $id = $this->sesion->getUsuarioActual();
        $where = [new DataBaseWhere('idusuario', $id)];
        if (!$this->usuario->siExiste($where)) {
            $this->sesion->terminarSesion();
            $this->redirect('');
            return;
        }

        // Cargamos el usuario
        $this->usuario->cargarPorId($id);

        // Si se tiene una sesión iniciada no debe acceder al login
        if ($this->esPaginaLogin()) {
            $ruta = $this->sitios_predeterminados[$this->usuario->rol];
            $this->redirect($ruta);
            return;
        }

        // Si es pública lo dejamos tal cual
        if ($this->esPublica()) {
            Tools::ImprimirEnLog('SessionController', 'manejarRutasConSesion', TipoMensaje::Seguimiento->value, "El controlador es público ({$paginaActual})");
            return;
        }

        // Si no es pública verificamos si hay permiso según rol
        if ($this->estaAutorizado()) {
            Tools::ImprimirEnLog('SessionController', 'manejarRutasConSesion', TipoMensaje::Seguimiento->value, "El usuario tiene permiso para el controlador ({$paginaActual})");
            return;
        }

        // Si no se encuentra autorizado se le movera a una página predeterminada
        Tools::ImprimirEnLog('SessionController', 'manejarRutasConSesion', TipoMensaje::Seguimiento->value, "El usuario tiene permiso para el controlador ({$paginaActual})");
        $ruta = $this->sitios_predeterminados['no-authorized'];
        $this->redirect($ruta);
    }

    /**
     * Realiza la comprobación y redirección tomando en cuenta
     * que no existe una sesión iniciada
     */
    private function manejarRutasSinSesion()
    {
        Tools::ImprimirEnLog('SessionController', 'manejarRutasSinSesion', TipoMensaje::Seguimiento->value, 'No existe sesión');

        // Si no existe una sesión, ifual validamos si es pública
        if ($this->esPublica()) {
            Tools::ImprimirEnLog('SessionController', 'manejarRutasSinSesion', TipoMensaje::Seguimiento->value, 'El controlador es público');
            return;
        }

        // Si no hay sesión y la página no es pública regresará al login
        $this->redirect('');
    }

    private function existeSesion(): bool
    {
        if (!$this->sesion->exist())
            return false;

        $id = $this->sesion->getUsuarioActual();

        return isset($id);
    }

    /**
     * Revisa la página actual para verificar si es el login
     */
    private function esPaginaLogin(): bool
    {
        $paginaActual = $this->getPaginaActual();
        $paginaActual = preg_replace("/\?.*/", "", $paginaActual); //omitir get info

        if (strtolower($paginaActual) === 'login')
            return true;

        if (strtolower($paginaActual) === '')
            return true;

        return false;
    }

    private function esPublica(): bool
    {
        $paginaActual = $this->getPaginaActual();
        $paginaActual = preg_replace("/\?.*/", "", $paginaActual); //omitir get info

        $sitio = $this->sitios[$paginaActual] ?? null;
        if (isset($sitio) && $sitio['acceso'] === 'public')
            return true;

        return false;
    }

    private function estaAutorizado(): bool
    {
        $paginaActual = $this->getPaginaActual();
        $paginaActual = preg_replace("/\?.*/", "", $paginaActual); //omitir get info

        $ruta = $this->sitios[$paginaActual] ?? null;

        // Verificamos que la ruta no este vacia
        if (empty($ruta))
            return false;

        // Verificamos si la página no tiene roles
        $sinRoles = empty($ruta['roles']);

        // Verificamos si la página posee el rol del usuario
        $estaElRol = in_array($this->usuario->rol, $ruta['roles']);

        // Si la ruta no tiene distinción de roles o el Rol tiene el permiso regresará true
        if ($sinRoles || $estaElRol)
            return true;

        return false;
    }

    private function getPaginaActual()
    {
        $actual_link = trim("$_SERVER[REQUEST_URI]");
        $url = explode('/', $actual_link);
        Tools::ImprimirEnLog('SessionController', 'getPaginaActual', TipoMensaje::Seguimiento->value, "{$actual_link} url => {$url[2]}");
        return $url[2];
    }
}
