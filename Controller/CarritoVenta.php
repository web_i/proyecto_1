<?php

namespace Controller;

use Controller\Base\SessionController;
use Utils\Carrito;
use Utils\Database_2;
use Utils\TipoMensaje;
use Utils\Tools;

class CarritoVenta extends SessionController
{
    /**
     * It returns the name of the model that will be used to store the data
     * 
     * @return string The name of the model.
     */
    public function getModeloPrincipal(): string
    {
        return '';
    }

    public function getVistaPrincipal(): string
    {
        return 'ventas/viewCart';
    }

    /**
     * Como se va a utilizar una vista distinta al index, cargo el render
     */
    public function render()
    {
        $accion = $_REQUEST['action'] ?? null;
        if (!empty($accion)) {
            $this->aplicarAccion($accion, $_POST);
            unset($_REQUEST['action']);
        }
        $this->view->render($this->getVistaPrincipal(), [], false);
        Tools::ImprimirEnLog($this->getClassDireccion(), 'render', TipoMensaje::Seguimiento->value, 'Carga de la vista (' . $this->getVistaPrincipal() . ')');
    }

    protected function getClassDireccion(): string
    {
        return (string) static::class;
    }

    protected function aplicarAccion(string $accion, array $data = [], string $vista = '')
    {
        switch ($accion) {
            case 'addToCart':
                return $this->agregarAlCarrito();
            case 'removeCartItem':
                return $this->eliminarDelCarrito();
            case 'updateCartItem':
                return $this->actualizarAlCarrito();
            case 'placeOrder':
                return $this->placeOrder();
        }
    }

    public function placeOrder()
    {
        $database = new Database_2();
        $cart = new Carrito();
        $db = $database->db;

        if ($_REQUEST['action'] == 'placeOrder' && $cart->total_items() > 0 && !empty($_SESSION['sessCustomerID'])) {
            // insert order details into database
            $insertOrder = $db->query("INSERT INTO orders (customer_id, total_price, created, modified) VALUES ('" . $_SESSION['sessCustomerID'] . "', '" . $cart->total() . "', '" . date("Y-m-d H:i:s") . "', '" . date("Y-m-d H:i:s") . "')");

            if ($insertOrder) {
                $orderID = $db->insert_id;
                $sql = '';
                // get cart items
                $cartItems = $cart->contents();
                foreach ($cartItems as $item) {
                    $sql .= "INSERT INTO order_items (order_id, product_id, quantity) VALUES ('" . $orderID . "', '" . $item['id'] . "', '" . $item['qty'] . "');";
                }
                // insert order items into database
                $insertOrderItems = $db->multi_query($sql);

                if ($insertOrderItems) {
                    $cart->destroy();
                    $params = ['id' => $orderID];
                    $ruta = "VentaCompletada";
                    $this->redirect($ruta, $params);
                } else {
                    $ruta = "Checkout";
                    $this->redirect($ruta);
                }
            } else {
                $ruta = "Checkout";
                $this->redirect($ruta);
            }
        }
    }

    public function eliminarDelCarrito()
    {
        $database = new Database_2();
        $cart = new Carrito();
        $db = $database->db;

        $deleteItem = $cart->remove($_REQUEST['idproducto']);
        $ruta = "CarritoVenta";
        $this->redirect($ruta);
    }

    public function actualizarAlCarrito()
    {
        $database = new Database_2();
        $cart = new Carrito();
        $db = $database->db;

        $itemData = array(
            'rowid' => $_REQUEST['idproducto'],
            'qty' => $_REQUEST['qty']
        );
        $updateItem = $cart->update($itemData);
        echo $updateItem ? 'ok' : 'err';
    }

    public function agregarAlCarrito()
    {
        $database = new Database_2();
        $cart = new Carrito();
        $db = $database->db;

        if ($_REQUEST['action'] == 'addToCart' && !empty($_REQUEST['idproducto'])) {
            $productID = $_REQUEST['idproducto'];
            // get product details
            $query = $db->query("SELECT * FROM productos WHERE idproducto = " . $productID);
            $row = $query->fetch_assoc();
            $itemData = array(
                'id' => $row['idproducto'],
                'name' => $row['nombre'],
                'price' => $row['precio'],
                'qty' => 1
            );

            $insertItem = $cart->insert($itemData);
            return;
        } else {
            $this->redirect('Venta');
        }
    }
}
