<?php

namespace Controller;

use Controller\Base\BaseController;

class Errores extends BaseController
{
    /**
     * It returns the name of the model that will be used to store the data
     * 
     * @return string The name of the model.
     */
    public function getModeloPrincipal(): string
    {
        return 'Login';
    }

    public function getVistaPrincipal(): string
    {
        return 'errores';
    }

    protected function getClassDireccion(): string
    {
        return (string) static::class;
    }

    protected function aplicarAccion(string $accion, array $data = [], string $vista = '')
    {
    }
}
