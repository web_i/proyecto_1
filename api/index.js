// Exportamos lasopciones directas de node/npm
const express = require('express');
const app = express();

const mysql = require('mysql');
const myconn = require('express-myconnection');

// Exmportamos las configuraciones
const config = require('./config/config.js');

// exportamos el código propio
const rutes = require('./src/rutes.js');
const rutes_products = require('./src/rutes_products.js');
const rutes_customers = require('./src/rutes_customers.js');
const rutes_suppliers = require('./src/rutes_suppliers.js');

// Establecemos que usaremos la confuguración
const DBOp = {
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: '',
    database: 'umg_proyecto_propio'
};

// Empezamos con la instancía de la API 
app.use(express.json());
app.use(myconn(mysql, DBOp, 'single'));

app.use('/api', rutes);
app.use('/api/clientes', rutes_customers);
app.use('/api/productos', rutes_products);
app.use('/api/proveedores', rutes_suppliers);

const port = config.app.port;
app.listen(port, () => console.log(`Escuchando en puerto ${port}`));