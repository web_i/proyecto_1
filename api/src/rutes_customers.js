const express = require('express');
const response = require('./response.js');

const rutes = express.Router();

rutes.get('/', (req, res) => {
    req.getConnection((err, connection) => {
        if (err)
            return response.error(null, res, err);

        connection.query('SELECT * FROM customers', (err, rows) => {
            if (err)
                return response.error(null, res, err);

            return res.send(rows);
        });
    });
});

rutes.get('/:id', (req, res) => {
    req.getConnection((err, connection) => {
        if (err)
            return response.error(null, res, err);

        connection.query("SELECT * FROM customers where id = ? ", req.params.id, (err, rows) => {
            if (err)
                return response.error(null, res, { message: 'No encontrado', body: err });

            return response.success(null, res, rows);
        });
    });
});

rutes.post('/', (req, res) => {
    req.getConnection((err, connection) => {
        if (err)
            return response.error(null, res, err);

        connection.query("INSERT INTO customers set ? ", req.body, (err, rows) => {
            if (err)
                return response.error(null, res, err);

            response.success(null, res, 'Agregado correctamente');
        });
    });
});

rutes.delete('/:id', (req, res) => {
    req.getConnection((err, connection) => {
        if (err)
            return response.error(null, res, err);

        connection.query("DELETE FROM customers where id = ? ", req.params.id, (err, rows) => {
            if (err)
                return response.error(null, res, { message: 'No encontrado', body: err });

            return response.success(null, res, 'Eliminado correctamente');
        });
    });
});

module.exports = rutes;