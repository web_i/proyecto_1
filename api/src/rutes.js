const express = require('express');
const response = require('./response.js');

const rutes = express.Router();

rutes.get('/', (req, res) => {
    let respuesta_general = {
        nombre: "Grupo Desarrollo Web",
        rutas_disponibles: [
            "clientes",
            "productos",
            "proveedores"
        ]
    };

    return response.success(null, res, respuesta_general);
});

module.exports = rutes;