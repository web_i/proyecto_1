const express = require('express');
const response = require('./response.js');

const rutes = express.Router();

rutes.get('/', (req, res) => {
    req.getConnection((err, connection) => {
        if (err)
            return response.error(null, res, err);

        connection.query('SELECT * FROM proveedores', (err, rows) => {
            if (err)
                return response.error(null, res, err);

            return res.send(rows);
        });
    });
});

rutes.get('/:idproveedor', (req, res) => {
    req.getConnection((err, connection) => {
        if (err)
            return response.error(null, res, err);

        connection.query("SELECT * FROM proveedores where idproveedor = ? ", req.params.idproveedor, (err, rows) => {
            if (err)
                return response.error(null, res, { message: 'No encontrado', body: err });

            return response.success(null, res, rows);
        });
    });
});

rutes.post('/', (req, res) => {
    req.getConnection((err, connection) => {
        if (err)
            return response.error(null, res, err);

        connection.query("INSERT INTO proveedores set ? ", req.body, (err, rows) => {
            if (err)
                return response.error(null, res, err);

            response.success(null, res, 'Agregado correctamente');
        });
    });
});

rutes.delete('/:idproveedor', (req, res) => {
    req.getConnection((err, connection) => {
        if (err)
            return response.error(null, res, err);

        connection.query("DELETE FROM proveedores where idproveedor = ? ", req.params.idproveedor, (err, rows) => {
            if (err)
                return response.error(null, res, { message: 'No encontrado', body: err });

            return response.success(null, res, 'Eliminado correctamente');
        });
    });
});

module.exports = rutes;