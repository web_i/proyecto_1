exports.success = function (req, res, message, status = 200) {
    res.status(status);
    res.send({
        success: true,
        status: status,
        body: message || 'Ok'
    });
}

exports.error = function (req, res, message, status = 404) {
    res.status(status);
    res.send({
        success: false,
        status: status,
        body: message || 'Upsi! Ocurrió un error'
    });
}