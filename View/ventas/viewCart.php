<?php
// initializ shopping cart class

use Utils\Carrito;
use Utils\Database_2;

$cart = new Carrito;

function actualizarCarrito($id, $cantidad)
{
    $database = new Database_2();
    $cart = new Carrito();
    $db = $database->db;

    $itemData = array(
        'rowid' => $_REQUEST['idproducto'],
        'qty' => $_REQUEST['qty']
    );
    $updateItem = $cart->update($itemData);
    echo $updateItem ? 'ok' : 'err';
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>CARRITO</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        .container {
            padding: 50px;
        }

        input[type="number"] {
            width: 20%;
        }
    </style>
    <script>
        function updateCartItem(obj, id) {
            $.get("CarritoVenta", {
                action: "aplicarAccionAjax",
                idproductoid: id,
                qty: obj.value
            }, function(data) {
                if (data == 'ok') {
                    location.reload();
                } else {
                    alert('Cart update failed, please try again.');
                }
            });
        }
    </script>
</head>
</head>

<body>
    <div class="container">
        <h1>Carrito de compra</h1>
        <table class="table">
            <thead>
                <tr>
                    <th>PRODUCTO</th>
                    <th>PRECIO</th>
                    <th>CANTIDAD</th>
                    <th>SUBTOTAL</th>
                    <th> </th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($cart->total_items() > 0) {
                    //get cart items from session
                    $cartItems = $cart->contents();
                    foreach ($cartItems as $item) {
                ?>
                        <tr>
                            <td><?php echo $item["name"]; ?></td>
                            <td><?php echo 'Q' . $item["price"] . ''; ?></td>
                            <td><input type="number" class="form-control text-center" value="<?php echo $item["qty"]; ?>" readonly></td>
                            <td><?php echo 'Q' . $item["subtotal"] . ''; ?></td>
                            <td>
                                <a href="<?php echo constant('URL') . 'CarritoVenta' ?>?action=removeCartItem&idproducto=<?php echo $item["rowid"]; ?>" class="btn btn-danger" onclick="return confirm('¿Está seguro?')"><i class="glyphicon glyphicon-trash"></i></a>
                            </td>
                        </tr>
                    <?php }
                } else { ?>
                    <tr>
                        <td colspan="5">
                            <p>EL CARRO ESTA VACIO.....</p>
                        </td>
                    <?php } ?>
            </tbody>
            <tfoot>
                <tr>
                    <td><a href="<?php echo constant('URL') . 'Venta' ?>" class="btn btn-warning"><i class="glyphicon glyphicon-menu-left"></i> Cotinuar comprando</a></td>
                    <td colspan="2"></td>
                    <?php if ($cart->total_items() > 0) { ?>
                        <td class="text-center"><strong>TOTAL <?php echo 'Q' . $cart->total() . ''; ?></strong></td>
                        <td><a href="<?php echo constant('URL') . 'Checkout' ?>" class="btn btn-success btn-block">Verificar <i class="glyphicon glyphicon-menu-right"></i></a></td>
                    <?php } ?>
                </tr>
            </tfoot>
        </table>
    </div>
</body>

</html>