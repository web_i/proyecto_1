<?php
// include database configuration file

use Utils\Database_2;

$database = new Database_2();
$db = $database->db;

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Proyecto DW</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        .container {
            padding: 50px;
        }

        .cart-link {
            width: 100%;
            text-align: right;
            display: block;
            font-size: 22px;
        }
    </style>
</head>
</head>

<body>
    <div class="container">
        <h1><a href="<?php echo constant('URL') ?>"><img width="50" heigth="50" src="./favicon.png">Productos</a></h1>
        <a href="<?php echo constant('URL') . 'Checkout' ?>" class="cart-link" title="View Cart"><i class="glyphicon glyphicon-shopping-cart"></i></a>
        <div id="products" class="row list-group">
            <?php
            //get rows query
            $query = $db->query("SELECT * FROM productos ORDER BY idproducto DESC LIMIT 10");
            if ($query->num_rows > 0) {
                while ($row = $query->fetch_assoc()) {
            ?>
                    <div class="item col-lg-4">
                        <div class="thumbnail">
                            <div class="caption">
                                <h4 class="list-group-item-heading"><?php echo $row["codproducto"]; ?></h4>
                                <p class="list-group-item-text"><?php echo $row["nombre"]; ?></p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="lead"><?php echo 'Q' . $row["precio"] . ''; ?></p>
                                    </div>
                                    <div class="col-md-6">
                                        <a class="btn btn-success" href="<?php echo constant('URL') . 'CarritoVenta' ?>?action=addToCart&idproducto=<?php echo $row["idproducto"]; ?>">Agregar al carrito</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }
            } else { ?>
                <p>Productos no encontrados.....</p>
            <?php } ?>
        </div>
    </div>
</body>

</html>