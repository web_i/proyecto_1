<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>Inicio vnd</title>
    <link rel="shortcut icon" href="./favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel='stylesheet' href='./Assets/icons/css/all.min.css'>
    <link rel="stylesheet" href="./Assets/css/menu_admin.css">
</head>

<body>
    <!-- partial:index.partial.html -->
    <main class="site-wrapper">
        <div class="pt-table desktop-768">
            <div class="pt-tablecell page-home relative" style="background-image: url(https://img.freepik.com/vector-premium/fondo-tecnologico-panal-azul_23-2148271326.jpg);
    background-position: center;
    background-size: cover;">
                <div class="overlay"></div>
                <div class="container">
                    <form action="<?php echo constant('URL') . 'HomeU' ?>" method="POST">
                        <input style="display: none;" readonly="true" type="text" value="cerrarsesion" name="action">
                        <button class="item" type="submit" name="accion" value="cerrar_sesion">CERRAR SESION</button>
                    </form>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8">
                            <div class="page-title  home text-center">
                                <span class="heading-page"> PUNTO DE VENTA
                                </span>
                                <p class="mt20">MENU PRINCIPAL PARA USUARIOS VENDEDORES.</p>
                                <p class="mt90">BIENVENIDO.</p>
                            </div>

                            <div class="hexagon-menu clear">
                                <div class="hexagon-item">
                                    <div class="hex-item">
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                    </div>
                                    <div class="hex-item">
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                    </div>
                                    <a class="hex-content" href="<?php echo constant('URL') . 'Venta' ?>">
                                        <span class="hex-content-inner">
                                            <span class="icon">
                                                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                            </span>
                                            <span class="title">SISTEMA DE FACTURACION</span>
                                        </span>
                                        <svg viewBox="0 0 173.20508075688772 200" height="200" width="174" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M86.60254037844386 0L173.20508075688772 50L173.20508075688772 150L86.60254037844386 200L0 150L0 50Z" fill="#1e2530"></path>
                                        </svg>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- partial -->
    <script src="./Assets/js/base/jquery.js"></script>
    <script src="./Assets/js/menu_vendedor.js"></script>
</body>

</html>