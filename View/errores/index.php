<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
	<!-- CSS only -->
	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="./Assets/css/errores.css" />
	<title>Oops</title>
</head>

<body>
	<div class="vertical-center">
		<form class="container" action="<?php echo constant('URL') ?>" method="POST">
			<div id="notfound" class="text-center ">
				<h1>😮</h1>
				<h2>Oops! La página no se ha encontrado</h2>
				<p>Aplaquesé</p>
				<button type="submit" name="action" value="go_home"><a>Regresar al inicio</a></button>
			</div>
		</form>
	</div>
</body>

</html>