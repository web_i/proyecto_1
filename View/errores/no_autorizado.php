<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <!-- CSS only -->
    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="./Assets/icons/css/all.min.css" />
    <link type="text/css" rel="stylesheet" href="./Assets/css/errores.css" />
    <link type="text/css" rel="stylesheet" href="./Assets/css/no_autorizado.css" />
    <title>Denegado!</title>
</head>

<body>
    <div class="vertical-center">
        <form class="container" action="<?php echo constant('URL') ?>" method="POST">
            <div id="notfound" class="text-center ">
                <h1 class="icono"><i class="fa-solid fa-road-lock"></i></h1>
                <h2>Usted no posee los permisos para ingresar a está página</h2>
                <p>Aplaquesé</p>
                <button class="btn btn-primary" type="submit"><a>Regresar al inicio</a></button>
            </div>
        </form>
    </div>
</body>

</html>