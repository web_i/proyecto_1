<!DOCTYPE html>
<html lang="en">

<?php

use Model\GrupoClienteModel;

?>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>EditCliente</title>

    <!-- El icono de pestaña -->
    <link rel="shortcut icon" href="./favicon.png" type="image/x-icon">

    <!-- Estilos e Icono -->
    <link rel='stylesheet' href='./Assets/icons/css/all.min.css'>
    <link href="./Assets/css/menu_lateral.min.css" rel="stylesheet">
    <link href="./Assets/css/menu_lateral.css" rel="stylesheet">

    <!-- Custom styles for this page -->
    <link href="./vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <h4><img width="50" heigth="50" src="./favicon.png"></h4>
                </div>
                <div class="sidebar-brand-text mx-3">Proyecto DW</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="<?php echo constant('URL') . 'DashboardAdmin' ?>">
                    <i class="fa-solid fa-house"></i>
                    <span>Inicio</span></a>
            </li>

            <!-- Nav Item - Dashboard -->
            <li class="nav-item" style="margin-top: -1.5em;">
                <a class="nav-link" href="<?php echo constant('URL') . 'MiPerfil' ?>">
                    <i class="fa-solid fa-user"></i>
                    <span>Mi perfil</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Interface
            </div>

            <!-- Nav Item - General Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseGeneral" aria-expanded="true" aria-controls="collapseGeneral">
                    <i class="fa-solid fa-building-user"></i>
                    <span>General</span>
                </a>
                <div id="collapseGeneral" class="collapse" aria-labelledby="headingGeneral" data-parent="#accordionSidebar">
                    <div class="bg-custom py-2 collapse-inner rounded">
                        <a class="collapse-item" href="<?php echo constant('URL') . 'ListSucursal' ?>"><i class="fa-solid fa-warehouse"></i></i> Sucursales</a>
                        <a class="collapse-item" href="<?php echo constant('URL') . 'ListUsuario' ?>"><i class="fa-solid fa-users"></i> Usuarios</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Inventarios Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="<?php echo constant('URL') . 'ListProducto' ?>" data-toggle="collapse" data-target="#collapseInventarios" aria-expanded="true" aria-controls="collapseInventarios">
                    <i class="fa-solid fa-boxes-stacked"></i>
                    <span>Inventarios</span>
                </a>
                <div id="collapseInventarios" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-custom py-2 collapse-inner rounded">
                        <a class="collapse-item" href="<?php echo constant('URL') . 'ListCategoria' ?>"><i class="fa-solid fa-layer-group"></i> Categorias</a>
                        <a class="collapse-item" href="<?php echo constant('URL') . 'ListProducto' ?>"><i class="fa-solid fa-boxes-stacked"></i> Productos</a>
                        <a class="collapse-item" href="<?php echo constant('URL') . 'ListExistencia' ?>"><i class="fa-solid fa-dolly"></i> Existencias</a>
                    </div>
                </div>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">
            <div class="sidebar-heading">
                Compras
            </div>

            <!-- Nav Item - Proveedores Menu -->
            <li class="nav-item">
                <a class="nav-link" href="<?php echo constant('URL') . 'ListProveedor' ?>">
                    <i class="fa-solid fa-users"></i>
                    <span>Proveedores</span></a>
            </li>

            <!-- Nav Item - DocsCompras Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseDocsCompras" aria-expanded="true" aria-controls="collapseDocsCompras">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>Docs. Compras</span>
                </a>
                <div id="collapseDocsCompras" class="collapse" aria-labelledby="headingDocsCompras" data-parent="#accordionSidebar">
                    <div class="bg-custom py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Facturación:</h6>
                        <a class="collapse-item" href="<?php echo constant('URL') . 'ListFacturaProveedor' ?>"><i class="fa-solid fa-file-invoice-dollar"></i> Facturas</a>
                        
                        <div class="collapse-divider"></div>
                        <h6 class="collapse-header">Pagos:</h6>
                        <a class="collapse-item" href="<?php echo constant('URL') . 'ListDocPorPagar' ?>"><i class="fa-solid fa-file-invoice-dollar"></i> Documentos por pagar</a>
                        <a class="collapse-item" href="<?php echo constant('URL') . 'ListNotaCreditoProveedor' ?>"><i class="fa-solid fa-file-invoice"></i> Notas de crédito</a>
                    </div>
                </div>
            </li>

            <!-- Heading -->
            <hr class="sidebar-divider">
            <div class="sidebar-heading">
                Ventas
            </div>

            <!-- Nav Item - Clientes Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link active" href="#" data-toggle="collapse" data-target="#collapseClientes" aria-expanded="true" aria-controls="collapseClientes">
                    <i class="fa-solid fa-users"></i>
                    <span>Clientes</span>
                </a>
                <div id="collapseClientes" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-custom py-2 collapse-inner rounded">
                        <a class="collapse-item" href="<?php echo constant('URL') . 'ListGrupoCliente' ?>"><i class="fa-solid fa-users-between-lines"></i> Grupos</a>
                        <a class="collapse-item active" href="<?php echo constant('URL') . 'ListCliente' ?>"><i class="fa-solid fa-users"></i> Clientes</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - DocsVentas Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseDocsVentas" aria-expanded="true" aria-controls="collapseDocsVentas">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>Docs. Ventas</span>
                </a>
                <div id="collapseDocsVentas" class="collapse" aria-labelledby="headingDocsVentas" data-parent="#accordionSidebar">
                    <div class="bg-custom py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Facturación:</h6>
                        <a class="collapse-item" href="<?php echo constant('URL') . 'ListFacturaCliente' ?>"><i class="fa-solid fa-file-invoice-dollar"></i> Facturas</a>
                        
                        <div class="collapse-divider"></div>
                        <h6 class="collapse-header">Cobros:</h6>
                        <a class="collapse-item" href="<?php echo constant('URL') . 'ListDocPorCobrar' ?>"><i class="fa-solid fa-file-invoice-dollar"></i> Documentos por cobrar</a>
                        <a class="collapse-item" href="<?php echo constant('URL') . 'ListNotaCreditoCliente' ?>"><i class="fa-solid fa-file-invoice"></i>Notas de crédito</a>
                    </div>
                </div>
            </li>

            <!-- Heading -->
            

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content" class="bg-custom">

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <div class="row justify-content-center">
                        <div class="col-md-10 col-lg-6 col-xl-5 order-2 order-lg-1">
                            <p class="text-center h1 fw-bold mb-4" style="color: #ffffff;">Creación de cliente</p>

                            <form class="mx-1 mx-md-4" action="<?php echo constant('URL') . 'EditCliente' ?>" method="POST">
                                <div class="d-flex flex-row align-items-center mb-4">
                                    <div class="form-outline flex-fill mb-0">
                                        <label class="form-label" for="mame">Nombre</label>
                                        <input type="text" id="name" name="name" class="form-control" require />
                                    </div>
                                </div>

                                <div class="d-flex flex-row align-items-center mb-4">
                                    <div class="form-outline flex-fill mb-0">
                                        <label class="form-label" for="email">Correo</label>
                                        <input type="email" id="email" name="email" class="form-control" require/>
                                    </div>
                                </div>

                                <div class="d-flex flex-row align-items-center mb-4">
                                    <div class="form-outline flex-fill mb-0">
                                        <label class="form-label" for="phone">Teléfono</label>
                                        <input id="phone" name="phone" class="form-control" require/>
                                    </div>
                                </div>

                                <div class="d-flex flex-row align-items-center mb-4">
                                    <div class="form-outline flex-fill mb-0">
                                        <label class="form-label" for="idgrupo">Grupo de Clientes</label>
                                        <br>
                                        <select name="idgrupo" class="select form-select col-12">
                                            <?php
                                            $grupos = new GrupoClienteModel();
                                            foreach ($grupos->obtenerTodos() as $grupo) {
                                                $id = $grupo['idgrupo'];
                                                $nombre = $grupo['nombre'];
                                                echo "<option value=\"{$id}\">{$nombre}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="d-flex flex-row align-items-center mb-4">
                                    <div class="form-outline flex-fill mb-0">
                                        <label class="form-label" for="address">Dirección</label>
                                        <input type="text" id="address" name="address" class="form-control" />
                                    </div>
                                </div>

                                <div class="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
                                    <button type="submit" name="accion" value="crear_cliente" class="btn btn-primary btn-lg">Crear cliente</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-custom">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>8° Semestre - Desarrollo web 2022 - Equipo Umizoomi </span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
    </div>
    <!-- End of Page Wrapper -->

    <!-- Bootstrap core JavaScript-->
    <script src="./vendor/jquery/jquery.min.js"></script>
    <script src="./vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="./vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="./Assets/js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="./vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="./vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="./Assets/js/demo/datatables-demo.js"></script>

</body>

</html>