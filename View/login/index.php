<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Login</title>
    <link rel="shortcut icon" href="./favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" href="./Assets/css/mensajes_errores.css">
    <link rel="stylesheet" href="./Assets/css/login.css">
</head>

<body>
    <!--- form starts here -->
    <div class="main-box">
        <div class="inner-container">
            <form action="<?php constant('URL') ?>" class="box" method="post">
                <h1>Login</h1>
                <input type="text" name="username" placeholder="Nombre de usuario o correo" />
                <input type="password" name="pass" placeholder="Contraseña" />
                <button class="btn-login" name="accion" value="iniciar_sesion">Login</button>
                </p>
            </form>
        </div>
    </div>
    <!--- form ends here -->
    </div>
    <script src="./Assets/js/login.js"></script>
</body>

</html>