<?php

namespace Utils;

use mysqli;

class Database_2
{
    //Create connection and select DB
    public $db;

    public function __construct()
    {
        $this->db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

        if ($this->db->connect_error) {
            die("Unable to connect database: " . $this->db->connect_error);
        }
    }
}
