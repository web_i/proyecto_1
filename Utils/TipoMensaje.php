<?php

namespace Utils;

/**
 * De momento esta clase enum es unicamente de pruebas
 */
enum TipoMensaje: string
{
    case Error = 'error';
    case Advertencia = 'warning';
    case Notificacion = 'notice';
    case Confirmacion = 'success';
    case Seguimiento = 'tracing';
}
