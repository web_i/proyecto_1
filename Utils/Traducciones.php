<?php

namespace Utils;

class Traducciones
{
    /** Archivo con las traducciones */
    private $diccionario;

    public function __construct()
    {
        $data = file_get_contents("./Translation/traducciones.json");
        $this->diccionario = json_decode($data, true);
    }

    /**
     * Realiza una traducción según sea requerido
     */
    public function trans(string $cadena = '')
    {
        return $this->diccionario[$cadena] ?? $cadena;
    }
}
