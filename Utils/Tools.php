<?php

namespace Utils;

class Tools
{
    /**
     * Función encargada de Imprimir en el log los mensajes deseados
     * 
     * Las condiciones que posee se cambian en el archivo ./Config/config.php
     */
    public static function ImprimirEnLog(string $clase, string $funcion, string $tipo = 'notice', string $contenido = '')
    {
        if (ONLY_ERROR_LOG === true && strtolower($tipo) !== TipoMensaje::Error->value)
            return;

        if (SKIP_TRACING_LOG === true && strtolower($tipo) === TipoMensaje::Seguimiento->value)
            return;

        error_log("{$clase}::{$funcion}() -> {$tipo} => {$contenido}");
    }
}
