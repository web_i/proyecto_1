<?php

namespace Utils;

enum RolesDisponibles: string
{
    case Administrador = 'admin';
    case Vendedor = 'vendedor';
    case Cajero = 'cajero';
}