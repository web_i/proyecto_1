<?php

namespace Core;

use Model\UsuarioModel;

class View
{
    public $usuario;

    public function __construct()
    {
        $sesion = new Sesion();
        if (empty($sesion->getUsuarioActual()))
            return;

        // Si no existe una sesión saldremos
        $this->usuario = new UsuarioModel();
        $this->usuario->cargarPorId($sesion->getUsuarioActual());
    }

    /**
     * Carga el archivo para la vista de la página
     * @param string $nombre El nombre de la vista que se encuentra en View
     * @param array $data La información inicial a pasar a la vista
     * @param bool $usar_index Si tiene true, cargará el index.php de forma predeterminada
     */
    function render(string $nombre, $data = [], bool $usar_index = true)
    {
        if (empty($nombre))
            return;

        if ($usar_index)
            $nombre .= '/index';

        $this->data = $data;
        require_once 'View/' . $nombre . '.php';
    }

    /**
     * Redirecciona la vista a otro controlador
     */
    public function redirect(string $ruta, $mensajes = [])
    {
        $data = [];
        $params = '';

        foreach ($mensajes as $key => $mensaje)
            $data[] = $key . '=' . $mensaje;

        $params = join('&', $data);
        if (!empty($params))
            $params = '?' . $params;

        header('Location: ' . constant('URL') . $ruta . $params);
    }

    public function aplicarAccionAjax()
    {
        $accion = $_REQUEST['action'];
        var_dump($accion);
        exit();
    }
}
