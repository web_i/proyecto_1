<?php

namespace Core;

class Sesion
{
    private $nombre_sesion = 'user';

    public function __construct()
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
    }

    /**
     * Asigna el usuario a la sesión
     */
    public function setUsuarioActual($user_id)
    {
        $_SESSION[$this->nombre_sesion] = (string) $user_id;
    }

    /**
     * Obtiene el username del usuario actual
     */
    public function getUsuarioActual()
    {
        return $_SESSION[$this->nombre_sesion] ?? NULL;
    }

    /**
     * Finaliza la sesión y borra todo lo almacenado
     */
    public function terminarSesion()
    {
        session_unset();
        session_destroy();
        unset($_SESSION);
    }

    /**
     * Verifica si existe una sesión creada
     */
    public function exist(): bool
    {
        return isset($_SESSION[$this->nombre_sesion]);
    }
}
