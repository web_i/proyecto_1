<?php

namespace Core;

require_once './Config/configdb.php';

use Exception;
use Utils\Tools;
use Core\DataBaseWhere;
use Utils\TipoMensaje;

/**
 * Clase encargada de realizar la conexión con la base de datos
 * y ejecutar las consultas del CRUD
 */
class DataBase
{
    /**
     * Almacena la conexión con la bd 
     * @var mixed */
    private $link;

    /** 
     * Se encarga de realizar la conexión con la base de datos 
     * @return bool $link La conexión establecida (En caso de 
     * ocurrir un error retorna false)
     */
    private function conectar(): bool
    {
        try {
            $this->link = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

            if (mysqli_connect_errno()) {
                return false;
            }

            return true;
        } catch (Exception $ex) {
            Tools::ImprimirEnLog('DataBase', 'conectar', TipoMensaje::Error->value, $ex->getMessage());
            return false;
        }

        return false;
    }

    /**
     * Corta la conexión con la base de datos
     */
    private function cortarConexion()
    {
        mysqli_close($this->link);
    }

    /**
     * Ejecuta una query personalizada
     */
    public function query(string $query)
    {
        // Verificamos que se encuentre todo lo necesario
        if (empty($query))
            return false;

        if (!$this->conectar())
            return false;

        // Ejecutamos el insert
        try {
            $result = mysqli_query($this->link, $query);
        } catch (Exception $ex) {
            $this->cortarConexion();
            Tools::ImprimirEnLog('DataBase', 'query', TipoMensaje::Error->value, $ex->getMessage());
            return false;
        }

        $this->cortarConexion();

        return $result;
    }

    /**
     * Realiza una insersión en la tabla seleccionada
     * @param string $tabla El nombre de la tabla con la que nos conectaremos
     * @param array $insertArray El array que posee las columas y valores a insertar
     * @return bool
     */
    public function insertar(string $tabla, array $insertArray): bool
    {
        // Verificamos que se encuentre todo lo necesario
        if (empty($tabla) || empty($insertArray['columnas']) || empty($insertArray['valores']))
            return false;

        $columnas = $insertArray['columnas'];
        $valores = $insertArray['valores'];

        if (!$this->conectar())
            return false;

        try {
            // Ejecutamos el insert
            $query = "INSERT INTO {$tabla} {$columnas} VALUES {$valores}";
            $result = mysqli_query($this->link, $query);
        } catch (Exception $ex) {
            Tools::ImprimirEnLog('DataBase', 'insertar', TipoMensaje::Error->value, $ex->getMessage());
            $this->cortarConexion();
            return false;
        }
        $this->cortarConexion();

        return $result;
    }

    /**
     * Realiza una insersión en la tabla seleccionada
     * @param string $tabla El nombre de la tabla con la que nos conectaremos
     * @param array $updateArray El array que posee las columas y valores a insertar
     * @return bool
     */
    public function actualizar(string $tabla, array $updateArray): bool
    {
        // Verificamos que se encuentre todo lo necesario
        if (empty($tabla) || empty($updateArray['valores']) || empty($updateArray['id']) || empty($updateArray['nombrecampo']))
            return false;

        $id = $updateArray['id'];
        $valores = $updateArray['valores'];
        $nombreCampoPrimario = $updateArray['nombrecampo'];

        if (!$this->conectar())
            return false;

        try {
            // Ejecutamos el update
            $query = "UPDATE {$tabla} SET {$valores} WHERE {$nombreCampoPrimario} = '{$id}'";
            $result = mysqli_query($this->link, $query);
        } catch (Exception $ex) {
            Tools::ImprimirEnLog('DataBase', 'actualizar', TipoMensaje::Error->value, $ex->getMessage());
            $this->cortarConexion();
            return false;
        }

        $this->cortarConexion();

        return $result;
    }

    /**
     * Realiza una insersión en la tabla seleccionada
     * @param string $tabla El nombre de la tabla con la que nos conectaremos
     * @param array $where Un array con los where a cargar
     * @return array
     */
    public function seleccionar(string $tabla, array $wheres = []): array
    {
        if (empty($tabla))
            return [];

        if (!$this->conectar())
            return [];

        $query = "SELECT * FROM {$tabla}";
        if (!empty($wheres)) {
            $query .= " WHERE ";
            /** @var DataBaseWhere */
            foreach ($wheres as $key => $where) {
                if ($key !== array_key_last($wheres))
                    $query .= $where->getExtension();

                $query .= (string) $where;
            }
        }

        try {
            $queryResult = mysqli_query($this->link, $query);
        } catch (Exception $ex) {
            Tools::ImprimirEnLog('DataBase', 'seleccionar', TipoMensaje::Error->value, $ex->getMessage());
            $this->cortarConexion();
            return [];
        }

        $this->cortarConexion();

        if (empty($queryResult))
            return [];

        $valores = [];
        while ($reg = mysqli_fetch_array($queryResult)) {
            $valores[] = $reg;
        }

        return $valores;
    }

    /**
     * It deletes a row from a table in a database
     * 
     * @param string tabla El nombre de la tabla
     * @param string nombre_campo El nombre de la llave primaria.
     * @param string id El valor de la llave primaria
     * 
     * @return bool Si se elimino o no.
     */
    public function eliminar(string $tabla, string $nombre_campo, string $id)
    {
        // Verificamos que se encuentre todo lo necesario
        if (empty($tabla) || empty($nombre_campo) || empty($id))
            return false;

        if (!$this->conectar())
            return false;

        try {
            // Ejecutamos el update
            $query = "DELETE FROM {$tabla} WHERE {$nombre_campo} = '{$id}'";
            $result = mysqli_query($this->link, $query);
        } catch (Exception $ex) {
            Tools::ImprimirEnLog('DataBase', 'eliminar', TipoMensaje::Error->value, $ex->getMessage());
            $this->cortarConexion();
            return false;
        }

        $this->cortarConexion();

        return $result;
    }
}
