<?php

namespace Core;

class DataBaseWhere
{
    /** El nombre del campo */
    private $campo;

    /** El valor del campo */
    private $valor;

    /** La condición a cargar */
    private $condicion;

    /** La extensión a emplear con este where AND o OR */
    private $extension;

    /**
     * It takes a string, a value, a string, and a string, and returns nothing
     * 
     * @param string campo The field name
     * @param mixed valor The value to be compared
     * @param string condicion The condition to be used in the query.
     * @param string extension AND or OR
     */
    public function __construct(string $campo, $valor, string $condicion = '=', string $extension = 'AND')
    {
        $this->campo = $campo;
        $this->valor = $valor;
        $this->condicion = $condicion;
        $this->extension = $extension;
    }

    /** Retorna la union que tiene el where con otros */
    public function getExtension()
    {
        return " {$this->extension} ";
    }

    public function __toString()
    {
        return "{$this->campo} {$this->condicion} '{$this->valor}'";
    }
}
