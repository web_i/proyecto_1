<?php

namespace Core;

use Controller\Errores;
use Controller\Login;

class App
{
    public function __construct()
    {
        $url = $_GET['url'] ?? '';
        $url = rtrim($url, '/');
        $url = explode('/', $url); // usuario / ruta [0]

        // Verificamos si existe controlador
        if (empty($url[0])) {
            error_log("APP::construct-> no hay controlador especificado");;

            // Instanciamos (El require se encuentra en el index)
            $controlador = new Login();
            $controlador->loadModel($controlador->getModeloPrincipal());
            $controlador->render();
            return false;
        }

        // Formateamos el nombre del archivo (Controlador a cargar)
        $archivoControlador = 'Controller/' . $url[0] . '.php';

        // Si no se encuentra el archivo se mostrará la página de errores
        if (!file_exists($archivoControlador)) {
            $controlador = new Errores();

            $controlador->loadModel();
            $controlador->render();
            return;
        }

        /** 
         * Debido a la carga automatica de "require_once" que se posee index
         * de la raiz, cambiaremos el "/" de la url a "\" que es cómo lo manejan
         * las rutas "namespace" y "use" 
         */
        $rutaControlador = str_replace('/', '\\', 'Controller/' . $url[0]);
        $controlador = new $rutaControlador();
        $controlador->loadModel($controlador->getModeloPrincipal());
        $controlador->render();
    }
}
