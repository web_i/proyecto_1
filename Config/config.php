<?php

#region Globales generales
define('ONLY_ERROR_LOG', FALSE);

// Omitir los logs que se aplican desde el código, para seguir los pasos que hace el sistema
define('SKIP_TRACING_LOG', FALSE);

define('URL', 'http://localhost/ProyectoDW/');
#endregion

#region Manejo de log
error_reporting(E_ALL);

ini_set('ignore_repeated_errors', TRUE);
ini_set('display_errors', FALSE);
ini_set('log_errors', TRUE);

ini_set("error_log", "./php-error.log");
error_log(" --------------------- Inicio de aplicación web");
#endregion